import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.2.0.RELEASE"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    kotlin("jvm") version "1.3.50"
    kotlin("plugin.spring") version "1.3.50"

}

group = "net.gowri"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val developmentOnly by configurations.creating
configurations {
    runtimeClasspath {
        extendsFrom(developmentOnly)
    }
}

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
    maven { url = uri("https://packages.confluent.io/maven/") }
    jcenter()
}

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath("com.commercehub.gradle.plugin:gradle-avro-plugin:0.17.0")
    }
}


apply(plugin = "com.commercehub.gradle.plugin.avro")

extra["springCloudVersion"] = "Hoxton.RC1"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.cloud:spring-cloud-starter-netflix-hystrix")
    implementation("org.springframework.cloud:spring-cloud-stream")
    implementation("org.springframework.cloud:spring-cloud-stream-schema:2.2.0.RELEASE")
    implementation("org.springframework.cloud:spring-cloud-stream-binder-kafka")
//    implementation("org.springframework.kafka:spring-kafka")

    compile("io.confluent:common-config:5.2.1")
    compile("io.confluent:kafka-avro-serializer:5.2.1")
    compile("io.confluent:kafka-schema-registry-client:5.2.1")
    compile("org.apache.avro:avro:1.9.1")
    compile("io.github.microutils:kotlin-logging:1.7.6")

    developmentOnly("org.springframework.boot:spring-boot-devtools")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("org.springframework.cloud:spring-cloud-stream-test-support")
    testImplementation("org.springframework.kafka:spring-kafka-test")
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

sourceSets {
    main {
        java.srcDir("${buildDir}/generated/java")
    }
}


tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.register<com.commercehub.gradle.plugin.avro.GenerateAvroJavaTask>("generateAvro") {
    source = fileTree("src/main/resources/avsc")
    setOutputDir(file("${buildDir}/generated/java"))
}

tasks.register<Exec>("kafkaUp") {
    group = "kafka"
    commandLine("confluent", "local", "start")
//    parseCommandLineArguments(kotlin.collections.listOf("/kfup"),)
}

tasks.register<Exec>("kafkaDown") {
    group = "kafka"
    commandLine("confluent", "local", "stop")
//    parseCommandLineArguments(kotlin.collections.listOf("/kfup"),)
}

//tasks.getByName("test"){
//    dependsOn("kafkaUp")
//}
//
//tasks.getByName("build"){
//    dependsOn("kafkaDown")
//}


tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
    source(tasks["generateAvro"].outputs)
}





