package net.gowri.kafkaproducer.api

import io.confluent.gowri.basicavro.Payment
import mu.KotlinLogging
import net.gowri.kafkaproducer.dto.ProducerRequest
import net.gowri.kafkaproducer.dto.ProducerResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Source
import org.springframework.kafka.support.Acknowledgment
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.Message
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component

@EnableBinding(Source::class)
@Component
class PaymentProducer (val source: Source) {

    private val logger = KotlinLogging.logger {}

    @SendTo(Source.OUTPUT)
    fun ticktock(message: Message<Any>): Payment {
        return Payment("no1", 100.00)
    }

    fun sendNow() : ProducerResponse{
        return try {
            source.output().send(MessageBuilder.withPayload(Payment("no2",200.00)).build())
            ProducerResponse("successful")
        }catch (e : Exception){
            logger.error(e) {"Error writing to partition:  $e"}
            ProducerResponse("failed")
        }finally {
            logger.info { "produced message from rest end point." }
        }
    }

    fun sendRequest(producerRequest: ProducerRequest): ProducerResponse {
        return try {
            source.output().send(MessageBuilder.withPayload(Payment(producerRequest.paymentId,producerRequest.paymentAmount)).build())
            ProducerResponse("successful")
        }catch (e : Exception){
            logger.error(e) {"Error writing request to partition:  $e"}
            ProducerResponse("failed")
        }finally {
            logger.info { "processed request and  message sent from rest end point for " + producerRequest.paymentId }
        }
    }
}