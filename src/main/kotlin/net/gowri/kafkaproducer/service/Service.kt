package net.gowri.kafkaproducer.service

import net.gowri.kafkaproducer.api.PaymentProducer
import net.gowri.kafkaproducer.dto.ProducerRequest
import net.gowri.kafkaproducer.dto.ProducerResponse
import org.springframework.stereotype.Component

@Component
class Service(val paymentProducer: PaymentProducer) {

    fun processRequest(producerRequest: ProducerRequest): ProducerResponse {
        return paymentProducer.sendRequest(producerRequest)
    }
}