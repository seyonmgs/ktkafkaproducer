package net.gowri.kafkaproducer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient


@SpringBootApplication
@EnableSchemaRegistryClient
class KafkaproducerApplication

fun main(args: Array<String>) {
    runApplication<KafkaproducerApplication>(*args)
}
