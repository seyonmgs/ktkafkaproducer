package net.gowri.kafkaproducer.dto

data class ProducerRequest (val paymentId : String, val paymentAmount : Double)